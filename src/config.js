export let apiConfig = {
    api: null,
    apiName: 'api',
    apiUrl: '/api/',
    authAndRedirectUrl: '/api/auth-and-redirect/',
    debug: false
}

export class Config {

    get loginRedirect() {
        return `${apiConfig.authAndRedirectUrl}?url=${window.location.href.replace(window.location.hash, '')}`
    }

    get interceptors() {
        // TODO: add a hook to allow more of these to be added
        return {
            request: request => this.requestInterceptor(request),
            response: response => this.responseInterceptor(response)
        }
    }

    configure(api, incomingConfig) {
        apiConfig = Object.assign(apiConfig, incomingConfig)
        apiConfig.api = api

        this.addEndpoints()
    }

    addEndpoints() {
        apiConfig.api.registerEndpoint(apiConfig.apiName, config => {
            config
                .withBaseUrl(apiConfig.apiUrl)
                .withDefaults({
                    credentials: 'same-origin',
                    headers: {
                        'Accept': 'application/json'
                    }
                })
                .withInterceptor(this.interceptors)
        })
    }

    requestInterceptor(request) {
        return request
    }

    responseInterceptor(response) {
        if (response.status === 401) {
            window.location.href = this.loginRedirect
        }

        return response
    }

}
