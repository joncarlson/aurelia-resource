import { Config } from './config';

export function configure(aurelia, configCallback) {
    if (configCallback === undefined || typeof configCallback !== 'function') return;

    configCallback(aurelia.container.get(Config));
}

export * from './config';
export * from './resource';
export * from './response';